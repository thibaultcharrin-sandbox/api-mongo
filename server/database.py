import pymongo

def init_db():
        # CONNECTION STRING
    myclient = pymongo.MongoClient("mongodb://myuser:mypwd@mongo-db:27017/")
    db = myclient["my_database"]

    # FEED DB
    try:
        db.validate_collection("customers")
    except:
        db["customers"].insert_many([
            {"name":"thibault"},
            {"name":"jeremy"},
            {"name":"noelle"},
            {"name":"florian"},
            {"name":"michael"}
            ])