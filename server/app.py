from database import init_db
from flask import Flask, request
import pymongo

app = Flask(__name__)

@app.route('/<uuid>/')
def mongo_test(uuid):

    data = request.get_json(silent=True)
    app.logger.info("mongo-server.mongo_test." + str(uuid) + ".data = " + str(data))

    # CONNECTION STRING
    myclient = pymongo.MongoClient("mongodb://myuser:mypwd@mongo-db:27017/")
    db = myclient["my_database"]
    
    # QUERY
    filter = data["name"]
    query = db["customers"].find({"name": {"$regex": filter}},{"_id": 0})

    app.logger.info("mongo-server.mongo_test." + str(uuid) + ".find() = " + str(query))
    items = []
    for item in query:
        items.append(item)
    response = {"items": items}

    app.logger.info("mongo-server.mongo_test." + str(uuid) + ".response = " + str(response))

    return response

if __name__ == '__main__':
    init_db()
    app.run(debug=True, host='0.0.0.0', port=8080)