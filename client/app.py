from flask import Flask
import requests
import uuid

app = Flask(__name__)

# FUNCTIONS
@app.route('/<name>/')
def main(name):
    data = query_mongo(name)
    return data

def query_mongo(name):
    data = {
        "name": name
    }

    # CONNECTION STRING
    # generate uuid per request to avoid race condition
    my_uuid = "flask-api-" + str(uuid.uuid4())
    url = "http://mongo-server:8080/" + my_uuid + "/"
    
    # QUERY
    response = requests.get(url, json=data, headers={'Content-type': 'application/json'})
    app.logger.info("mongo-client.query_mongo_.response.status_code = " + str(response.status_code))
    
    # RESPONSE
    data = response.json()
    app.logger.info("mongo-client.query_mongo_.response.json() = " + str(response.json()))
    
    return data

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)

