#!/bin/bash

function buildApp() {
    cd $1
    ./build.sh
}

buildApp client/ &
buildApp server/ &
wait
echo -e "\nYou're all set!"